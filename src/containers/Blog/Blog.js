import React, { Component } from 'react';
import { Route } from 'react-router';
// import Axios from 'axios';
import uuid from 'uuid';

import './Blog.css';
import Posts from './Posts/Posts';

class Blog extends Component {

    addPostHandler = (post) => {
        const updatedPost = {...post, id: uuid.v1()}
        // console.log(updatedPost);
        this.setState((prevState) => ({
            posts: [...prevState.posts, updatedPost]
        }));
    }

    render () {
        
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/new-post">New Post</a></li>
                        </ul>
                    </nav>
                </header>
                <Route path="/" exact render={() => <Posts />}/>
                {/* 
                <section>
                    <NewPost addPost={this.addPostHandler}/>
                </section> */}
            </div>
        );
    }
}

export default Blog;