import Axios from '../../../axios';
import React, { Component } from 'react';

import './NewPost.css';

class NewPost extends Component {
    state = {
        title: '',
        body: '',
        author: 'Max'
    }

    postDataHandler = () => {
        Axios.post('/posts', {
            title: this.state.title,
            body: this.state.body,
            author: this.state.author
        }).then(response => {
            console.log(response);
            this.props.addPost(response.data);
            this.setState({
                title: '',
                body: ''
            })
        });
    }

    onChangeHandler = (e) => {
        this.setState({[e.target.name] : e.target.value});
    }

    render () {
        return (
            <div className="NewPost">
                <h1>Add a Post</h1>
                <label>Title</label>
                <input name="title" type="text" value={this.state.title} onChange={(e)=>this.onChangeHandler(e)} />
                <label>Body</label>
                <textarea name="body" rows="4" value={this.state.body} onChange={(e) => this.onChangeHandler(e)} />
                <label>Author</label>
                <select name="author" value={this.state.author} onChange={(e) => this.onChangeHandler(e)}>
                    <option value="Max">Max</option>
                    <option value="Manu">Manu</option>
                </select>
                <button onClick={this.postDataHandler}>Add Post</button>
            </div>
        );
    }
}

export default NewPost;