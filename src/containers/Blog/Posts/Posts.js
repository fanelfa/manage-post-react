import React, {Component} from 'react';
import Axios from '../../../axios';

import './Posts.css';

import Post from '../../../components/Post/Post';
import FullPost from '../FullPost/FullPost';


class Posts extends Component{
   state = {
      posts: [],
      selectedPost: null
   }

   componentDidMount() {
      Axios.get('/posts')
         .then(response => {
            const posts = response.data.slice(0, 4);
            const updatedPosts = posts.map(post => {
               return {
                  ...post,
                  author: 'Max'
               }
            });
            console.log(posts);
            this.setState({ posts: updatedPosts });
         })
         .catch(err => {
            // console.log(err);
            this.setState({ error: true });
         });
   }

   postSelectedHandler = (post) => {
      this.setState({ selectedPost: post });
   }

   render(){
      let posts = <p style={{ textAlign: 'center', color: 'red', fontWeight: 'bold' }}>Something went wrong!</p>
      if (!this.state.error) {
         posts = this.state.posts.map(post => {
            return <Post
               key={post.id}
               title={post.title}
               author={post.author}
               onClick={() => this.postSelectedHandler(post)} />
         });
      }
      return(
         <>
            <section className="Posts">
               {posts}
            </section>
            <section>
               <FullPost post={this.state.selectedPost}/>
            </section>
         </>
      );
   }
}

export default Posts;