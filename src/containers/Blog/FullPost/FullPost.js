import Axios from '../../../axios';
import React, { Component } from 'react';

import './FullPost.css';

class FullPost extends Component {
    deletePostHandler = (id) =>{
        Axios.delete('/posts/'+id)
            .then(response =>{
                console.log(response);
            });
    }
    render () {
        let post = <p style={{textAlign:'center'}}>Please select a Post!</p>;
        if(this.props.post){
            post = (
                <div className="FullPost">
                    <h1>{this.props.post.title}</h1>
                    <p>{this.props.post.body}</p>
                    <div className="Edit">
                        <button className="Delete" onClick={() => this.deletePostHandler(this.props.post.id)}>Delete</button>
                    </div>
                </div>
    
            );
        }
        return post;
    }
}

export default FullPost;