import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Axios from 'axios';


// Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
// Axios.defaults.headers.post['Content-Type'] = 'application/json';

Axios.interceptors.request.use(req => {
   // console.log(req);
   // bisa nambah config di sini
   return req;
}, error => {
   return Promise.reject(error);
});

Axios.interceptors.response.use(res => {
   // console.log(res);
   // bisa nambah config di sini
   return res;
}, error => {
   console.log(error);
   return Promise.reject(error);
});

ReactDOM.render(
   <React.StrictMode>
      <App />
   </React.StrictMode>,
   document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
